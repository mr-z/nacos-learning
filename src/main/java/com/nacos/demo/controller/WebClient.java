package com.nacos.demo.controller;

import com.alibaba.nacos.client.naming.utils.CollectionUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * @Author mr-z
 * @create 2019/11/9 14:51
 */
@RestController
public class WebClient {

    @PostMapping("/sayHello")
    public Mono<String> sayHello(@RequestBody Mono<String> mono, ServerHttpRequest request){
        HttpHeaders httpHeaders = request.getHeaders();
        StringBuilder sb = new StringBuilder();
        List<String> from = httpHeaders.get("from");
        if (!CollectionUtils.isEmpty(from)) {
            sb.append(from.get(0));
        }
        return mono.map(name ->
                sb.append("，来了！").append("你好").append(name).toString());
    }
}
